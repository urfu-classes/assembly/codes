org 0x100
use16

mov ax, 3
int 0x10

mov ax, 0xb800
mov es, ax

scan:
mov ah, 0
int 0x16

push ax

mov ah, 0x6
mov al, 0
mov bh, 0
mov ch, 0
mov cl, 0
mov dh, 0
mov dl, 79
int 10h

pop ax

mov [code_scan], ah
mov [code_ascii], al
push ax

xor bx, bx
mov bl, al
mov bh, 0x07

xor di, di
mov [es:di], bx

mov di, 2
mov bl, ';'
mov bh, 0x07
mov [es:di], bx

xor bx, bx
mov bl, [code_ascii]
mov ax, bx

mov si, code_ascii_str + 4
call write_code_string

mov di, 4
mov cx, 3
mov si, code_ascii_str
call write_code_buffer_video

mov bl, ';'
mov bh, 0x07
mov [es:di], bx
add di, 2

xor ax, ax
mov al, [code_scan]
mov si, code_scan_str + 4
call write_code_string

mov cx, 3
mov si, code_scan_str
call write_code_buffer_video

pop ax
cmp ah, 1
jne scan

int 0x20


write_code_string:
mov byte [di+0], 0
mov byte [di+2], 0
mov byte [di+4], 0
mov cx, 10
fill_digit:
xor dx, dx
div cx
add dl, 48
mov [si], dl
sub si, 2
cmp ax, 0
jne fill_digit
ret

write_code_buffer_video:
mov word ax, [si]
cmp al, 0
je next
mov [es:di], ax
add di, 2
next:
add si, 2
loop write_code_buffer_video
ret

code_ascii_str:
db 0, 0x07, 0, 0x07, 0, 0x07

code_scan_str:
db 0, 0x07, 0, 0x07, 0, 0x07

code_ascii:
db 0

code_scan:
db 0
